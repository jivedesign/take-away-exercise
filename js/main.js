$(document).ready(function() {

  var imageData;

  /** 
    * UI Elements
    */
  var ui = {
    wrapper: $(".gallery"),
    previousButton: $("#prev"),
    nextButton: $("#next"),
    imageTitle: $("#imageTitle"),
    imageDescription: $("#imageDescription"),
    imageContainer: $(".images")
  };

  /** 
    * Update Image Labels
    * ----
    * This function should set the #imageTitle and
    * #imageDescription elements to the title and
    * description of the .current visible image.
    */
  function updateLabel() { };

  /** 
    * Data Load Event handler
    * ----
    * Call this function when the images JSON data
    * has loaded. This function should:
    *   1. Create the DOM element for each image 
    *      and append it to the imageContainer element
    *   2. Assign the appropriate classes to the 
    *      image DOM elements to display the first image
    *   3. Call the updateLabel() function
    *   4. Remove the "loading" class from the gallery 
    *      wrapper.
    */
  function onDataLoad(data) { };

  /**
    * AJAX request
    * ----
    * Add an AJAX request to get the /data/images.json
    * file here, and call onDataLoad on success.



  /** 
    * Event Handlers
    * ----
    * Place all code that is triggered by
    * user interaction here.
    */
  function onPreviousClick() {
    switchImage("left");
  };

  function onNextClick() {
    switchImage("right");
  };

  // bind event handlers to their actions
  ui.previousButton.click(onPreviousClick);
  ui.nextButton.click(onNextClick);

  /** 
    * Switch image
    * ----
    * Gets the next or previous .image DOM element
    * relative to the .current image, then sets the
    * classes to make that element visible.
    */
  function switchImage(dir) {
    var currentImage = $('.gallery .current');
    var nextImage;
    switch (dir) {
      case "left":
        nextImage = currentImage.prev();
        if(nextImage.length === 0) {
          nextImage = $('.gallery .image').last();
        }
        currentImage.attr('class', 'image bounceOutRight');
        nextImage.attr('class', 'image current bounceInLeft');
        break;
      case "right":
        nextImage = currentImage.next();
        if(nextImage.length === 0) {
          nextImage = $('.gallery .image').first();
        }
        currentImage.attr('class', 'image bounceOutLeft');
        nextImage.attr('class', 'image current bounceInRight');
        break;
    }
    updateLabel();
  };

});